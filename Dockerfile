FROM centos:7
RUN yum -y install python3.7 python3-pip
RUN pip3 install flask flask_restful
COPY python-api.py /python_api/
CMD ["python3", "/python_api/python-api.py"]
